# VNAPI

#### Description


VNAPI 是由VNPY官方（http://www.vnpy.cn ）为公募基金、私募基金、职业交易团队、券商等专业金融机构提供一 手、合规、准确、实时的行情接口，各行情源均得到各交易所的合法授权。 VNPY官方在提供实时行情接口的同时，还提供部分数据的本地仿真回测接口VNAPI，仿真和接口和实时行情 接口完全一致，只需要替换DLL即可实现实盘和量化回测的切换（关于仿真回测接口更详细介绍见本文的第20 章）。 仿真回测接口和配套数据可免费下载,该量化交易回测方案已获国家发明专利。 由于仿真接口同时需要行情和交易接口的实现，而作为行情服务商不能提供交易接口，所以交易接口暂时按 上海期货交易所的CTP接口方式实现（上期所CTP接口同时提供了行情和交易，而A股程序化接口通常由券商按合 规要求提供），这样就完成了量化仿真回测的闭环。 对量化开发者而言，行情数据接口形式不同决定了策略开发模式的不同，而在采用不同交易接口实现时代码 仅有微小区别。所以在VNPY仿真回测后接入生产环境实盘时可按合规要求替换为自己交易接口即可简单实现。 

 

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
