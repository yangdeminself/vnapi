# VNAPI

#### 介绍

![效果](http://www.vnapi.cn/img2/s01.png)
VNAPI（ http://www.vnapi.cn ）是

VNPY官方（ http://www.vnpy.cn ）子品牌。

VNAPY为公募基金、私募基金、职业交易团队、券商等专业金融机构提供一手、合规、准确、实时的行情接口。

VNAPI提供的沪深A股Level、沪深A股Level2、港股的实时行情接口数据均收到交易所合法授权。除了实时行情接口以外，VNAPI还免费提供了配套的仿真回测接口公开下载。

VNAPI接口架构采用金融行业标准的上海期货交易所（以下简称上期）CTP接口架构，回调函数名称与CTP架构也较为一致。（注：上期CTP 接口支持国内8大合规交易所中的5所 ，支持商品期货、股指期货、商品期权、股指期权，但不提供证券A股行情，CTP架构已成为金融行业接口标准， 大部分券商给用户提供的行情接口都是基于模仿CTP架构设计的。）

为弥补上期CTP接口对沪深A股、港股等市场支持的不足，VNAPI接口采用CTP架构进行封装，提供了沪深A股Level1、沪深A股Level2、港股实时行情数据等实时行情订阅。

![效果](http://www.vnapi.cn/index_files/system.png)

除此以外，VNAPI还提供本地仿真回测接口VNAPI，仿真和接口和实时行情接口完全一致，只需要替换DLL即可实现实盘和量化回测的切换（关于仿真回测接口更详细介绍见本文的第20章）。

仿真回测接口和配套数据可免费下载,该量化交易回测方案已获国家发明专利。
![效果](http://www.vnapi.cn/img2/s06.jfif)
![效果](http://www.vnapi.cn/img2/s03.jpg)
![效果](http://www.vnapi.cn/img2/s02.jpg)
由于仿真接口同时需要行情和交易接口的实现，而作为行情服务商不能提供交易接口，所以交易接口暂时按上海期货交易所的CTP接口方式实现（上期所CTP接口同时提供了行情和交易，而A股程序化接口通常由券商按合规要求提供），这样就完成了量化仿真回测的闭环。

对量化开发者而言，行情数据接口形式不同决定了策略开发模式的不同，而在采用不同交易接口实现时代码仅有微小区别。所以在VNPY仿真回测后接入生产环境实盘时可按合规要求替换为自己交易接口即可简单实现。

![效果](http://www.vnapi.cn/img2/s04.gif)
标准CTP架构 C++ Demo架构图

![效果](http://www.vnapi.cn/img2/s05.gif)
仿真回测 C++ Demo架构图

上期CTP结口资源列表   http://www.virtualapi.cn/download.html 
实盘开通CTP接口       http://www.kaihucn.cn 

上期CTP原生接口只提供C+++ API, 其他编程语言都是由第三方个人或公司封装的框架，比如Python框架，JAVA框架，C#框架等。而VNAPI也计划在提供原生C++  API基础上会适当封装一些其他语言的开源框架，开源链接可以在 http://www.vnapi.cn 找到。

实时行情接口分为多个回调函数，除了仿真回测接口的M1分钟回调外，还有沪深A股Level1,沪深A股Level2，港股，期货实时数据，期货分钟数据等等。

关于Level2数据量的问题，每天大约40GB左右的数据量，低延时，全量推送，对深圳交易所可以根据逐笔成交和逐比委托还原全档盘口。

VNAPI仿真回测接口提供部分回调函数，如果需要完整的沪深A股Level1、沪深A股Level2数据回测，请通过 http://www.vnpy.cn 和 http://www.vnapi.cn 官网提供的联系方式进行联系。

关于仿真回测数据文件

VNAPI仿真数据回测接口的数据文件是在本地存储，由配置文件指定存储路径，API会自动读取路径数据文件进行行情数据回放。

kdata 目录下是沪深A股1分钟周期的K线数据，是和  http://www.vnapi.cn  仿真回测接口匹配数据。
setting目录下是个股回测用的数据文件列表，比如list_000001.csv是股票000001数据文件列表，数据文件列表指定数据文件按交易日由先到后排序，在仿真回测时会依次被仿真回测dll文件读取回放历史行情数据。

allfile.csv保存了kdata目录所有的数据文件，供备用。

实盘行情支持的行情订阅参数：
//上海交易所Level1行情
SubscribeMarketData (MARKET_SHANGHAILEVEL1);
//深圳交易所Level1行情
SubscribeMarketData (MARKET_SHENZHENLEVEL1);
//上海交易所Level2行情
SubscribeMarketData(MARKET_SHANGHAILEVEL2);
//深圳交易所Level2行情
SubscribeMarketData(MARKET_SHENZHENLEVEL2);
//上海分钟线
SubscribeMarketData (MARKET_SHENZHENBAR);
//港股行情
SubscribeMarketData(MARKET_HK);
//期货行情
SubscribeMarketData(MARKET_FUTRUE);

仿真回测支持的回调方法（免费）
///分钟数据回调
void OnRtnBarData(CThostFtdcBarDataField *pDepthMarketData) 
仿真回测支持的回调方法（收费）
//Level2 深圳行情
void OnRtnSZStockMarketL2Data(CThostFtdcSZStockMarketL2DataField *pDepthMarketData) 
//Level2 上海行情
void OnRtnSHStockMarketL2Data(CThostFtdcSHStockMarketL2DataField *pDepthMarketData) 
///A股行情通知
void OnRtnStockMarketData(CThostFtdcStockMarketDataField *pDepthMarketData)
///Level2委托通知
void OnRtnL2Order(CSecurityFtdcL2OrderField *pL2Order) 
///Level2成交通知
void OnRtnL2Trade(CSecurityFtdcL2TradeField *pL2Trade)

注：对沪深Level1和Levell2的仿真接口回调，请请通过  http://www.vnpy.cn  官网提供的联系方式进行联系。

实时行情支持的回调方法：
///分钟数据回调
void OnRtnBarData(CThostFtdcBarDataField *pDepthMarketData) 
//Level2 深圳行情
void OnRtnSZStockMarketL2Data(CThostFtdcSZStockMarketL2DataField *pDepthMarketData) 
//Level2 上海行情
void OnRtnSHStockMarketL2Data(CThostFtdcSHStockMarketL2DataField *pDepthMarketData) 
///期货行情通知
void OnRtnFutureMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) 
///A股行情通知
void OnRtnStockMarketData(CThostFtdcStockMarketDataField *pDepthMarketData)
///基金行情通知
void OnRtnFundMarketData(CThostFtdcFundMarketDataField *pDepthMarketData)
///基金行情通知
void OnRtnBondMarketData(CThostFtdcBondMarketDataField *pDepthMarketData)
///香港市场行情通知
void OnRtnHKStockMarketData(CThostFtdcHKStockMarketDataField *pDepthMarketData)
//指数行情
void OnRtnL2Index(CSecurityFtdcL2IndexField *pL2Index) 
///Level2委托通知
void OnRtnL2Order(CSecurityFtdcL2OrderField *pL2Order) 
///Level2成交通知
void OnRtnL2Trade(CSecurityFtdcL2TradeField *pL2Trade)

本技术文档下载
http://www.vnapi.cn/doc.pdf


#### 软件架构
软件架构说明
VNAPI采用和上海期货交易所CTP接口相同架构，也是采用了金融行业的接口标准。

#### 安装教程

1.  目前仅提供Windows API；
2.  数据格式采用CSV格式；

#### 使用说明

1.  VNAPI目前提供基于C++ API  的Demo，以后会Python,JAVA等框架，也有计划提供分布式回测框架。
     目前仿真是基于1分钟K线的，如果需要基于A股Level1和Level2仿真，请  http://www.vnpy.cn  联系。
     如需实时行情，也请按上述官方联系方式联系。
![效果](http://www.vnapi.cn/index_files/details_contribute.png)


2.  对Python接口，可以参考CTP的Python框架，比如VN.PY，Python等
3.  对JAVA接口，可参考CTP的JAVA框架，以后可能官方提供。
4.  对C#接口，可参考老版本的海风C#版CTP框架。
5. CTP原生接口可以在  http://www.virtualapi.cn/download.html  找到资源，提供链接是为了便于大家理解CTP架构，文档非常详细。



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
